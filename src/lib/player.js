import uuid from 'uuid/v4'

export class Player {
  constructor () {
    this.id = uuid()
    this.name = ''
    this.codeName = ''
    this.team = null
    this.position = null
  }
}