import uuid from 'uuid/v4'
import { TeamName } from './team_name'

export class Team {
  constructor () {
    this.id = uuid()
    this.name = new TeamName().name
    this.players = []
  }
}
